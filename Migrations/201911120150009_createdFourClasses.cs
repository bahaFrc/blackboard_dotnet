﻿namespace Blackboard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdFourClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AClasses",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ClassDescription = c.String(),
                        year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CourseDescription = c.String(),
                        CourseName = c.String(),
                        AClass_Id = c.String(maxLength: 128),
                        Prof_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AClasses", t => t.AClass_Id)
                .ForeignKey("dbo.Profs", t => t.Prof_Id)
                .Index(t => t.AClass_Id)
                .Index(t => t.Prof_Id);
            
            CreateTable(
                "dbo.Profs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ProfCIN = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        ProfPic = c.String(),
                        ProfDescription = c.String(),
                        ProfEmail = c.String(),
                        ProfPhone = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        StudentCIN = c.String(),
                        StudentName = c.String(),
                        StudentLastName = c.String(),
                        AClass_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AClasses", t => t.AClass_Id)
                .Index(t => t.AClass_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "AClass_Id", "dbo.AClasses");
            DropForeignKey("dbo.Courses", "Prof_Id", "dbo.Profs");
            DropForeignKey("dbo.Courses", "AClass_Id", "dbo.AClasses");
            DropIndex("dbo.Students", new[] { "AClass_Id" });
            DropIndex("dbo.Courses", new[] { "Prof_Id" });
            DropIndex("dbo.Courses", new[] { "AClass_Id" });
            DropTable("dbo.Students");
            DropTable("dbo.Profs");
            DropTable("dbo.Courses");
            DropTable("dbo.AClasses");
        }
    }
}
