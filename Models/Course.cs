﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Models
{
    public class Course
    {
        public string Id { get; set; }
        public string CourseDescription { get; set; }
        public string CourseName { get; set; }
        public Prof Prof { get; set; }
        public AClass AClass { get; set; }

    }
}
