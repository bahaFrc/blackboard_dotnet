﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Models
{
    public class Student
    {
        public string Id { get; set; }
        public string StudentCIN { get; set; }
        public string StudentName { get; set; }
        public string StudentLastName { get; set; }
        public AClass AClass { get; set; }
    }
}
