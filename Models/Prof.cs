﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Models
{
    public class Prof
    {
        public string Id { get; set; }
        public string ProfCIN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfPic { get; set; }
        public string ProfDescription { get; set; }
        public string ProfEmail { get; set; }
        public int ProfPhone { get; set; }
        public List<Course> Courses { get; set; }
    }
}