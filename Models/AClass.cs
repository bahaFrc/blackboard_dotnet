﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Models
{
    public class AClass
    {
        public AClass()
        {

        }
        public string Id { get; set; }
        public string ClassDescription { get; set; }
        public int year { get; set; }
        public List<Student> Students { get; set; }
        public List<Course> Courses { get; set; }
    }
}
