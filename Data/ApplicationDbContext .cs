﻿using Blackboard.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace Blackboard.Data
{
    public partial class ApplicationDbContext : DbContext
    {
        /*public ApplicationDbContext()
           : base("Name=MyContext")
        {
        }*/

        public DbSet<Student> Students { get; set; }
        public DbSet<AClass> Classes { get; set; }
        public DbSet<Prof> Profs { get; set; }
        public DbSet<Course> Courses { get; set; }
    }
}
